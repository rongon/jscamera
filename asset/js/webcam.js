

// preload shutter audio clip
var shutter = new Audio();
shutter.autoplay = false;
shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';


var time = 0;
setTimeout(take_snapshot, 2000);

var setTime = setInterval(take_snapshot, 3000);

function take_snapshot() {
    time = time + 1;
    if (time == 10) {
        clearInterval(setTime);
        document.querySelector("#message").innerHTML = "Session Time Out";
    } else {

        // play sound effect
        shutter.play();

        // take snapshot and get image data
        Webcam.snap(function (data_uri) {
            // display results in page
            document.getElementById('results').innerHTML =
                '<img id="imageprev" src="' + data_uri + '"/>';
        });
        saveSnap();
    }
}

function saveSnap() {
    // Get base64 value from <img id='imageprev'> source
    var base64image = document.getElementById("imageprev").src;

    Webcam.upload(base64image, 'upload.php', function (code, text) {
        document.querySelector("#message").innerHTML = "Save Successfully";
        document.getElementById("config").click();

    });

}