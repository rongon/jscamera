<?php
session_start();
var_dump($_SESSION);
?>
<style>
    #my_camera {
        width: 400px;
        height: 300px;
        border: 1px solid black;
        margin-bottom: 10px;
    }

    .margin {
        width: 50%;
        margin: 0 auto;
    }
</style>
<iframe src="https://demo.pondit.com/my/" height="200px" width="100%"></iframe>
<body onload="configure()">
    <div class="margin">
        <div id="message"></div>
        <div id="my_camera"></div>
        <div id="results"></div>
    </div>
</body>


<script>
    function configure() {
        Webcam.set({
            width: 400,
            height: 300,
            image_format: 'jpeg',
            jpeg_quality: 90
        });
        Webcam.attach('#my_camera');
    }
    
</script>
<script src="asset/js/webcam.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.26/webcam.min.js"></script>
